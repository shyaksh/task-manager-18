package ru.bokhan.tm.repository;

import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public void addAll(final List<User> userList) {
        users.addAll(userList);
    }

    @Override
    public void load(final List<User> userList) {
        clear();
        addAll(userList);
    }

    @Override
    public void clear() {
        users.clear();
    }


    @Override
    public User remove(final User user) {
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}