package ru.bokhan.tm.api.service;

import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll();

    void clear(String userId);

    void clear();

    void load(List<Task> tasks);

    List<Task> findAll(String userId);

    Task findById(String userId, String id);

    Task findByIndex(String userId, Integer index);

    Task findByName(String userId, String name);

    Task removeById(String userId, String id);

    Task removeByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

}