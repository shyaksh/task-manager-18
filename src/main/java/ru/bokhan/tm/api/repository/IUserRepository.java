package ru.bokhan.tm.api.repository;

import ru.bokhan.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    void addAll(List<User> userList);

    void load(List<User> userList);

    void clear();

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

}
