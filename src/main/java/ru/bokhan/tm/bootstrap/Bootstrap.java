package ru.bokhan.tm.bootstrap;

import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.*;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.command.auth.LoginCommand;
import ru.bokhan.tm.command.auth.LogoutCommand;
import ru.bokhan.tm.command.auth.RegistryCommand;
import ru.bokhan.tm.command.data.base64.DataBase64ClearCommand;
import ru.bokhan.tm.command.data.base64.DataBase64LoadCommand;
import ru.bokhan.tm.command.data.base64.DataBase64SaveCommand;
import ru.bokhan.tm.command.data.binary.DataBinaryClearCommand;
import ru.bokhan.tm.command.data.binary.DataBinaryLoadCommand;
import ru.bokhan.tm.command.data.binary.DataBinarySaveCommand;
import ru.bokhan.tm.command.data.json.DataJsonClearCommand;
import ru.bokhan.tm.command.data.json.DataJsonLoadCommand;
import ru.bokhan.tm.command.data.json.DataJsonSaveCommand;
import ru.bokhan.tm.command.data.xml.DataXmlClearCommand;
import ru.bokhan.tm.command.data.xml.DataXmlLoadCommand;
import ru.bokhan.tm.command.data.xml.DataXmlSaveCommand;
import ru.bokhan.tm.command.project.*;
import ru.bokhan.tm.command.system.*;
import ru.bokhan.tm.command.task.*;
import ru.bokhan.tm.command.user.*;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.system.UnknownCommandException;
import ru.bokhan.tm.repository.ProjectRepository;
import ru.bokhan.tm.repository.TaskRepository;
import ru.bokhan.tm.repository.UserRepository;
import ru.bokhan.tm.service.*;
import ru.bokhan.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IDomainService domainService = new DomainService(
            userService,
            projectService,
            taskService
    );

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private static final Class[] COMMANDS = new Class[]{
            AboutCommand.class, ExitCommand.class, HelpCommand.class,
            SystemInfoCommand.class, VersionCommand.class,

            LoginCommand.class, LogoutCommand.class, RegistryCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
            ProjectByIdRemoveCommand.class, ProjectByIndexRemoveCommand.class, ProjectByNameRemoveCommand.class,
            ProjectByIdUpdateCommand.class, ProjectByIndexUpdateCommand.class, ProjectByIdViewCommand.class,
            ProjectByIndexViewCommand.class, ProjectByNameViewCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class, TaskListCommand.class,
            TaskByIdRemoveCommand.class, TaskByIndexRemoveCommand.class, TaskByNameRemoveCommand.class,
            TaskByIdUpdateCommand.class, TaskByIndexUpdateCommand.class, TaskByIdViewCommand.class,
            TaskByIndexViewCommand.class, TaskByNameViewCommand.class,

            PasswordUpdateCommand.class, ProfileUpdateCommand.class, ProfileViewCommand.class,
            UserLockCommand.class, UserUnlockCommand.class, UserRemoveCommand.class,

            DataBinarySaveCommand.class, DataBinaryLoadCommand.class, DataBinaryClearCommand.class,
            DataBase64SaveCommand.class, DataBase64LoadCommand.class, DataBase64ClearCommand.class,
            DataXmlSaveCommand.class, DataXmlLoadCommand.class, DataXmlClearCommand.class,
            DataJsonSaveCommand.class, DataJsonLoadCommand.class, DataJsonClearCommand.class
    };

    private void initCommands() {
        for (Class clazz : COMMANDS) {
            try {
                final AbstractCommand command = (AbstractCommand) clazz.newInstance();
                registry(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initData() {
        final User user = userService.create("test", "test", "test@test.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(user.getId(), "UserTask1");
        taskService.create(user.getId(), "UserTask2");
        taskService.create(admin.getId(), "AdminTask1");
        taskService.create(admin.getId(), "AdminTask2");
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
    }

    public void run(final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        try {
            if (parseArguments(arguments)) System.exit(0);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        initCommands();
        initData();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArgument(final String argument) throws Exception {
        if (argument == null || argument.isEmpty()) return;
        final AbstractCommand command = getCommandByArgument(argument);
        if (command == null) return;
        command.execute();
    }

    public void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        final Role[] roles = command.roles();
        if (roles != null) {
            authService.checkRoles(command.roles());
        }
        command.execute();
    }

    public boolean parseArguments(final String[] arguments) throws Exception {
        if (arguments == null || arguments.length == 0) return false;
        final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    private AbstractCommand getCommandByArgument(final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (final AbstractCommand command : commands.values()) {
            if (argument.equals(command.argument())) return command;
        }
        return null;
    }

}