package ru.bokhan.tm.service;

import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.Domain;

public final class DomainService implements IDomainService {

    private final IUserService userService;

    private final IProjectService projectService;

    private final ITaskService taskService;

    public DomainService(
            final IUserService userService,
            final IProjectService projectService,
            final ITaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
    }

}
