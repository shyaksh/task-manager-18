package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.util.TerminalUtil;

public class ProjectByNameRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
