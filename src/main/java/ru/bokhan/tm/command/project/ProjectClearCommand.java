package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

}
