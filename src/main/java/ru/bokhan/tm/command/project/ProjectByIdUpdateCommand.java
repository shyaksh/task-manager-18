package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.util.TerminalUtil;

public class ProjectByIdUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
