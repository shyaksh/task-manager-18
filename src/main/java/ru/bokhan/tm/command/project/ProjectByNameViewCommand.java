package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.util.TerminalUtil;

public class ProjectByNameViewCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

}
