package ru.bokhan.tm.command.task;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.util.TerminalUtil;

public class TaskByIdUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
