package ru.bokhan.tm.command.task;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.util.TerminalUtil;

public class TaskByIndexViewCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-view-by-index";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

}
