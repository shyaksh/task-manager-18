package ru.bokhan.tm.command.data.base64;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove base64 binary data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
