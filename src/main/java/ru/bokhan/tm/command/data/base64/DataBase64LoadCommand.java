package ru.bokhan.tm.command.data.base64;

import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.file.Files;

public class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from base64 binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final File file = new File(DataConstant.FILE_BASE64);
        final byte[] fileBytes = Files.readAllBytes(file.toPath());
        final String base64 = new String(fileBytes);
        final byte[] bytes = new BASE64Decoder().decodeBuffer(base64);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        objectInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
