package ru.bokhan.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;

import java.io.FileInputStream;

public class DataXmlLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_XML);
        ObjectMapper mapper = new XmlMapper();
        final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        fileInputStream.close();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
