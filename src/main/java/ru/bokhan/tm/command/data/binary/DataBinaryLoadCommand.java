package ru.bokhan.tm.command.data.binary;

import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final File file = new File(DataConstant.FILE_BINARY);
        final FileInputStream fileInputStream = new FileInputStream(file);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
