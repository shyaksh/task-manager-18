package ru.bokhan.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;

import java.io.FileInputStream;

public class DataJsonLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        ObjectMapper mapper = new ObjectMapper();
        final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
