package ru.bokhan.tm.command.data.binary;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove binary data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY CLEAR]");
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
