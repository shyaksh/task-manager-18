package ru.bokhan.tm.command;

import ru.bokhan.tm.api.service.IServiceLocator;
import ru.bokhan.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute() throws Exception;

}
