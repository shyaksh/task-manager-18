package ru.bokhan.tm.command.system;

import ru.bokhan.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
